//==============================================================================
/*
 * This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 *
 * Medical Data Segmentation Toolkit (MDSTk)    \n
 * Copyright (c) 2003-2010 by Michal Spanel     \n
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2006/10/31                          \n
 *
 * File description:
 * - Sample slice processing module.
 */

#include "ProcessSlice.h"

// MDSTk
#include <MDSTk/Image/mdsSlice.h>

// STL
#include <iostream>
#include <sstream>


//==============================================================================
/*
 * Global constants.
 */

//! Module description.
const std::string MODULE_DESCRIPTION    = "Sample module processing input slice.";

//! Additional command line arguments.
//! - Colon separated list!
//const std::string MODULE_ARGUMENTS      = "a";

//! Additional arguments names.
//const std::string ARG_A                 = "a";

//! Default arguments values.
//const int DEFAULT_A                     = 1;


//==============================================================================
/*
 * Implementation of the class CProcessSlice.
 */
CProcessSlice::CProcessSlice(const std::string& sDescription)
    : mds::mod::CModule(sDescription)
{
    // Allow additional command line arguments
//    allowArguments(MODULE_ARGUMENTS);
}


CProcessSlice::~CProcessSlice()
{
}


bool CProcessSlice::startup()
{
    // Note
    MDS_LOG_NOTE("Module startup");

    // Test of existence of input and output channel
    if( getNumOfInputs() != 1 || getNumOfOutputs() != 1 )
    {
        MDS_CERR('<' << m_sFilename << "> Wrong number of input and output channels" << std::endl);
        return false;
    }

    // Command line argument - a
    //! - Sample code!
/*    m_iA = DEFAULT_A;
    m_Arguments.value(ARG_A, m_iA);
    if( m_iA > 100 )
    {
        MDS_CERR('<' << m_sFilename << "> Bad " << ARG_A <<  " parameter value: type -h for help" << std::endl);
        printUsage();
        return false;
    }*/

    // Command line argument - b
    //! - Sample code!
//    m_bB = m_Arguments.exists(ARG_B);

    // O.K.
    return true;
}


bool CProcessSlice::main()
{
    // Note
    MDS_LOG_NOTE("Module main function");

    // I/O channels
    mds::mod::CChannel *pIChannel = getInput(0);
    mds::mod::CChannel *pOChannel = getOutput(0);

    // Is any input?
    if( !pIChannel->isConnected() )
    {
        return false;
    }

    // Create a new slice
    mds::img::CSlicePtr spSlice(new mds::img::CSlice());

    // Wait for data
    if( pIChannel->wait(1000) )
    {
        // Read slice from the input channel
        if( readInput(pIChannel, spSlice.get()) )
        {
            // Create a new slice
            // - Copy of the input one
            mds::img::CSlicePtr spResult(new mds::img::CSlice(*spSlice));

            // Process the input slice
            // ...

            // Write the result to the output channel
            if( !writeOutput(pOChannel, spResult.get()) )
            {
                MDS_CERR('<' << m_sFilename << "> Failed to write the output slice" << std::endl);
            }
        }
        else
        {
            MDS_CERR('<' << m_sFilename << "> Failed to read input slice" << std::endl);
            return false;
        }
    }
    else
    {
        MDS_LOG_NOTE("Wait timeout");
    }

    // Returning true means to continue processing the input channel
    return true;
}


void CProcessSlice::shutdown()
{
    // Note
    MDS_LOG_NOTE("Module shutdown");
}


void CProcessSlice::writeExtendedUsage(std::ostream& Stream)
{
/*    MDS_CERR(std::endl);
    MDS_CERR("Extended usage: [-a iValue] [-b]" << std::endl);
    MDS_CERR("Options:" << std::endl);
    MDS_CERR("  -a  Description ..." << std::endl);    
    MDS_CERR("  -b  ..." << std::endl);*/
}


//==============================================================================
/*
 * Function main() which creates and executes console application.
 */
int main(int argc, char *argv[])
{
    // Creation of a module using smart pointer
    CProcessSlicePtr spModule(new CProcessSlice(MODULE_DESCRIPTION));

    // Initialize and execute the module
    if( spModule->init(argc, argv) )
    {
        spModule->run();
    }

    // Console application finished
    return 0;
}

