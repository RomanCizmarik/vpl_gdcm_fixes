# CMake module that tries to find Python extension includes and libraries.
#
# The following variables are set if Python extension is found:
#   PYTHON_EXTENSION_FOUND         - True when the Python extension include directory is found.
#   PYTHON_EXTENSION_INCLUDE_DIR   - The directory where Python extension include files are.
#   PYTHON_EXTENSION_LIBRARIES_DIR - The directory where Python extension libraries are.
#   PYTHON_EXTENSION_LIBRARIES     - List of all Python extension libraries.
#
# Usage:
#   In your CMakeLists.txt file do something like this:
#   ...
#   FIND_PACKAGE(PYTHON_EXTENSION)
#   ...
#   INCLUDE_DIRECTORIES(${PYTHON_EXTENSION_INCLUDE_DIR})
#   LINK_DIRECTORIES(${PYTHON_EXTENSION_LIBRARIES_DIR})
#   ...
#   TARGET_LINK_LIBRARIES( mytarget ${PYTHON_EXTENSION_LIBRARIES} )


# Initialize the search path
if( WIN32 )
  set( PYTHON_EXTENSION_DIR_SEARCH
       "${TRIDIM_3RDPARTY_DIR}"
       "${TRIDIM_MOOQ_DIR}"
       "${TRIDIM_MOOQ_DIR}/3DimPython"
       "${PYTHON_EXTENSION_ROOT_DIR}"
       "$ENV{PYTHON_EXTENSION_ROOT_DIR}"
       "$ENV{ProgramFiles}/3DimPython"
       C:/3DimPython
       D:/3DimPython
       )
  set( PYTHON_EXTENSION_SEARCHSUFFIXES
       include
       )
else( WIN32 )
  set( PYTHON_EXTENSION_DIR_SEARCH
       "${TRIDIM_MOOQ_DIR}"
       "${TRIDIM_MOOQ_DIR}/3DimPython"
       "${PYTHON_EXTENSION_ROOT_DIR}"
       "$ENV{3DimPython}"
       /usr/include
       /usr/local/include
       /opt/include
       /opt/local/include
       ~/3DimPython/include )
  set( PYTHON_EXTENSION_SEARCHSUFFIXES
       ""
       )
endif( WIN32 )


# Try to find PYTHON_EXTENSION include directory
find_path( PYTHON_EXTENSION_INCLUDE_DIR
           NAMES
           PyLib/runtime.h
           PATHS
           ${PYTHON_EXTENSION_DIR_SEARCH}
           PATH_SUFFIXES
           ${PYTHON_EXTENSION_SEARCHSUFFIXES}
           )


# Assume we didn't find it
set( PYTHON_EXTENSION_FOUND NO )
# Now try to get the library path
IF( PYTHON_EXTENSION_INCLUDE_DIR )

  # Look for the PYTHON_EXTENSION library path
  SET( PYTHON_EXTENSION_LIBRARIES_DIR ${PYTHON_EXTENSION_INCLUDE_DIR} )

  # Strip off the trailing "/include" in the path
  IF( "${PYTHON_EXTENSION_LIBRARIES_DIR}" MATCHES "/include$" )
    GET_FILENAME_COMPONENT( PYTHON_EXTENSION_LIBRARIES_DIR ${PYTHON_EXTENSION_LIBRARIES_DIR} PATH )
  ENDIF( "${PYTHON_EXTENSION_LIBRARIES_DIR}" MATCHES "/include$" )

  # Check if the 'lib' directory exists
  IF( EXISTS "${PYTHON_EXTENSION_LIBRARIES_DIR}/lib" )
    SET( PYTHON_EXTENSION_LIBRARIES_DIR ${PYTHON_EXTENSION_LIBRARIES_DIR}/lib )
  ENDIF( EXISTS "${PYTHON_EXTENSION_LIBRARIES_DIR}/lib" )
    

  

  # Add unversioned image lib name to the testing list
  list(APPEND PYTHON_EXTENSION_PYTHONLIB_NAMES libPython)
  list(APPEND PYTHON_EXTENSION_PYTHONLIB_DEBUG_NAMES libPythond)
 
  

  # Find release libraries
  FIND_LIBRARY( PYTHON_EXTENSION_libPython_LIBRARY
                NAMES ${PYTHON_EXTENSION_PYTHONLIB_NAMES}
                PATHS ${PYTHON_EXTENSION_LIBRARIES_DIR} ${PYTHON_EXTENSION_LIBRARIES_DIR_RELEASE}
                NO_DEFAULT_PATH NO_CMAKE_ENVIRONMENT_PATH
                NO_CMAKE_PATH NO_SYSTEM_ENVIRONMENT_PATH
                NO_CMAKE_SYSTEM_PATH )

  # Try to find debug libraries
  FIND_LIBRARY( PYTHON_EXTENSION_libPythond_LIBRARY
                NAMES ${PYTHON_EXTENSION_PYTHONLIB_DEBUG_NAMES}
                PATHS ${PYTHON_EXTENSION_LIBRARIES_DIR} ${PYTHON_EXTENSION_LIBRARIES_DIR_DEBUG}
                NO_DEFAULT_PATH NO_CMAKE_ENVIRONMENT_PATH
                NO_CMAKE_PATH NO_SYSTEM_ENVIRONMENT_PATH
                NO_CMAKE_SYSTEM_PATH )
  
  unset(PYTHON_EXTENSION_PYTHONLIB_NAMES)
  unset(PYTHON_EXTENSION_PYTHONLIB_DEBUG_NAMES)
  
               
  # Check what libraries was found
  IF( PYTHON_EXTENSION_libPython_LIBRARY AND PYTHON_EXTENSION_libPythond_LIBRARY )
    SET( PYTHON_EXTENSION_LIBRARIES
         optimized libPython
         debug libPythond)
    SET( PYTHON_EXTENSION_FOUND "YES" )
    
  ELSE( PYTHON_EXTENSION_libPython_LIBRARY AND PYTHON_EXTENSION_libPythond_LIBRARY )
 
    IF(PYTHON_EXTENSION_libPython_LIBRARY)
      SET( PYTHON_EXTENSION_LIBRARIES libPython )
      SET( PYTHON_EXTENSION_FOUND "YES" )
      
    ELSE( PYTHON_EXTENSION_libPython_LIBRARY )
    
      IF( PYTHON_EXTENSION_libPythond_LIBRARY )
        SET( PYTHON_EXTENSION_LIBRARIES  libPythond)
        SET( PYTHON_EXTENSION_FOUND "YES" )
      ENDIF( PYTHON_EXTENSION_libPythond_LIBRARY )
      
    ENDIF( PYTHON_EXTENSION_libPython_LIBRARY )
    
  ENDIF( PYTHON_EXTENSION_libPython_LIBRARY AND PYTHON_EXTENSION_libPythond_LIBRARY )
  
ENDIF( PYTHON_EXTENSION_INCLUDE_DIR )

# Using shared or static library?
IF( PYTHON_EXTENSION_FOUND )
#    OPTION( PYTHON_EXTENSION_USE_SHARED_LIBRARY "Should shared library be used?" OFF )
#    IF( PYTHON_EXTENSION_USE_SHARED_LIBRARY )
#      ADD_DEFINITIONS( -DMDS_LIBRARY_SHARED )
#    ELSE( PYTHON_EXTENSION_USE_SHARED_LIBRARY )
      ADD_DEFINITIONS( -DMDS_LIBRARY_STATIC )
#    ENDIF( PYTHON_EXTENSION_USE_SHARED_LIBRARY )
    if( APPLE )
        add_definitions( -D_MACOSX )
    else()
        if( UNIX )
            add_definitions( -D_LINUX )
        endif()
    endif()
ENDIF( PYTHON_EXTENSION_FOUND )

MARK_AS_ADVANCED( PYTHON_EXTENSION_INCLUDE_DIR
                  PYTHON_EXTENSION_libPython_LIBRARY
                  PYTHON_EXTENSION_libPythond_LIBRARY )


# display help message
IF( NOT PYTHON_EXTENSION_FOUND )
  # make FIND_PACKAGE friendly
  IF( NOT PYTHON_EXTENSION_FIND_QUIETLY )
    IF( PYTHON_EXTENSION_FIND_REQUIRED )
      MESSAGE( FATAL_ERROR "PYTHON_EXTENSION required but some headers or libs not found. Please specify it's location with PYTHON_EXTENSION_ROOT_DIR variable.")
      SET( PYTHON_EXTENSION_ROOT_DIR "" CACHE PATH "PYTHON_EXTENSION root dir" )
    ELSE( PYTHON_EXTENSION_FIND_REQUIRED )
      MESSAGE(STATUS "ERROR: PYTHON_EXTENSION was not found.")
    ENDIF( PYTHON_EXTENSION_FIND_REQUIRED )
  ENDIF( NOT PYTHON_EXTENSION_FIND_QUIETLY )
ENDIF( NOT PYTHON_EXTENSION_FOUND )

