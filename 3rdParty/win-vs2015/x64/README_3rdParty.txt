================================================================================
- This file is part of
- 
- VPL - Voxel Processing Library
- Copyright 2017 3Dim Laboratory s.r.o.
- All rights reserved.
- 
- Use of this file is governed by a BSD-style license that can be
- found in the LICENSE file.
================================================================================

This is a package of prebuilt 3rd party libraries for use with VPL.

- Unpack content of this archive into the 'VPL/3rdParty' source directory
  or somewhere else...
  
- Enable the VPL_PREFER_SUPPLIED_3RDPARTY_LIBS option when running the CMake
  utility, and modify the variable VPL_3RDPARTY_DIR if necessary.
