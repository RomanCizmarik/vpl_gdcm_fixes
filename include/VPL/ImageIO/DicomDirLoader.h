//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#ifndef VPL_DICOMDIRLOADER_H
#define VPL_DICOMDIRLOADER_H

#include <VPL/ImageIO/ImageIOExport.h>
#include <VPL/ImageIO/DicomSliceLoaderVPL.h>
#include <VPL/ImageIO/DicomSliceLoaderGDCM.h>
#include <VPL/System/FileBrowser.h>
#include <VPL/Image/DensityVolume.h>


namespace vpl
{
namespace img
{
//==============================================================================
/*!
* Loads all DICOM images from directory, and make volume.
*/
class VPL_IMAGEIO_EXPORT CDicomDirLoader
{
public:

    CDicomDirLoader()
    {
        imageCount = 0;
    }
    //! Get number of success loaded DICOM files.
    int getNumOfImages() 
    {
        return imageCount;
    }
    //! Loads data from DICOM files in direcotry, including image data, patient information, etc.
    bool loadDicomDir(std::string directory, std::string filemask = "*.dcm");

    //! Create density volume from loaded dicom files.
    bool makeDensityVolume(vpl::img::CDensityVolume& dstVolume, int borderValue = vpl::img::CPixelTraits<vpl::img::CDVolume::tVoxel>::getPixelMin());

private:
    std::vector<vpl::img::CSlicePtr> dicomSlices;
    int imageCount;
};
}
}

#endif