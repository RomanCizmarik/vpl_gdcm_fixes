//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#include <VPL/ImageIO/DicomDirLoader.h>

namespace vpl
{
namespace img
{

bool CDicomDirLoader::loadDicomDir(std::string directory, std::string filemask)
{
    // Initialize the file browser
    vpl::sys::CFileBrowserPtr spFileBrowser;

    // Get the current directory
    std::string CWD = spFileBrowser->getDirectory();

    // Change the current directory
    if (!spFileBrowser->setDirectory(directory))
    {
        return false;
    }
    // Load all JPEG files in the directory
    vpl::sys::CFileBrowser::SFileAttr File;
    bool bResult = spFileBrowser->findFirst(filemask, File);

    

    // Load all JPEG files in the directory
    for (; bResult; bResult = spFileBrowser->findNext(File))
    {
        // Smart pointer to DICOM slice
        vpl::img::CDicomSlicePtr spSlice(new vpl::img::CDicomSlice);

        // Read JPEG image
        bool bOK = false;

#if defined( VPL_USE_GDCM )
        CDicomSliceLoaderGDCM loader;
        bOK = loader.loadDicom(File.m_sName, *spSlice, true);
#else
        CDicomSliceLoaderVPL loader;
        bOK = loader.loadDicom(File.m_sName, *spSlice, true);
#endif
        // Check for an error...
        if (!bOK)
        {
            continue;
        }

        // Another image loaded...
        dicomSlices.push_back((CSlicePtr)spSlice);
        ++imageCount;
    }
    // Restore the current working directory
    spFileBrowser->setDirectory(CWD);

    // Was any file found?
    if (imageCount == 0)
    {
        return false;
    }

    return true;
}

bool CDicomDirLoader::makeDensityVolume(vpl::img::CDensityVolume& dstVolume, int borderValue)
{
    // Make volume
    if (dstVolume.makeVolume(dicomSlices))
    {
        // Smart pointer to DICOM slice
        vpl::img::CDicomSlicePtr spSlice(new vpl::img::CDicomSlice);

        return true;
    }
    return false;
}

} // namespace img
} // namespace vpl
