//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2003/11/01                       
 *
 * Description:
 * - Testing of the vpl::CThread class.
 */

#include <VPL/System/ScopedLock.h>
#include <VPL/System/Sleep.h>
#include <VPL/System/Thread.h>

// STL
#include <iostream>


//==============================================================================
/*
 * Global constants.
 */

//! The number of threads
#define THREADS         5

//! Delay after releasing mutex
#define DELAY           50

//! Smart pointer to critical section
vpl::sys::CMutex        Mutex;


//==============================================================================
/*!
 * Thread routine
 */
VPL_THREAD_ROUTINE(thread)
{
    int id = *(reinterpret_cast<int *>(pThread->getData()));

    VPL_THREAD_MAIN_LOOP
    {
        {
            vpl::sys::tScopedLock Guard(Mutex);
            std::cout << "Thread: " << id << std::endl;
        }
        vpl::sys::sleep(DELAY + DELAY * id);
    }

    vpl::sys::tScopedLock Guard(Mutex);
    std::cout << "Thread: " << id << " terminated" << std::endl;

    return 0;
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    // Threads
    vpl::sys::CThread *pThreads[THREADS];
    int piThreadsId[THREADS];

    // Creation of all ppThreads
    for( int i = 0; i < THREADS; i++ )
    {
        piThreadsId[i] = i;
        pThreads[i] = new vpl::sys::CThread(thread, (void *)&piThreadsId[i], true);
    }

    // Sleep
    vpl::sys::sleep(10000);

    // Destroy all ppThreads
    for( int i = 0; i < THREADS; i++ )
    {
        pThreads[i]->terminate(true, 1000);
        delete pThreads[i];
    }

    return 0;
}

