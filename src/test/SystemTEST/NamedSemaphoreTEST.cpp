//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2003/11/04                       
 *
 * Description:
 * - Testing of the vpl::CNamedSemaphore class.
 */

#include <VPL/System/Sleep.h>
#include <VPL/System/Thread.h>
#include <VPL/System/NamedSemaphore.h>
#include <VPL/System/ScopedLock.h>

#include <cstring>
#include <cassert>
#include <cstdlib>

// STL
#include <iostream>
#include <string>


//==============================================================================
/*
 * Global constants and variables.
 */

//! The number of threads
#define THREADS     5

//! Delay after releasing semaphore
#define DELAY       200

//!    Named semaphore used for mutual exclusion
vpl::sys::CNamedSemaphore *pSemaphore   = NULL;

//! Name of the sempahore
const std::string SEMAPHORE_NAME        = "1234";


//==============================================================================
/*!
 * Generates random number
 */
unsigned random(const unsigned uiMax)
{
    return (1 + (unsigned int)(((double)rand() / RAND_MAX) * uiMax));
}


//==============================================================================
/*!
 * Thread routine
 */
VPL_THREAD_ROUTINE(thread)
{
    int id = *(reinterpret_cast<int *>(pThread->getData()));

    VPL_THREAD_MAIN_LOOP
    {
        vpl::sys::CScopedLock<vpl::sys::CNamedSemaphore> Guard1(*pSemaphore);
        std::cout << "Thread: " << id << std::endl;
        vpl::sys::sleep(random(DELAY));
    }

    vpl::sys::CScopedLock<vpl::sys::CNamedSemaphore> Guard2(*pSemaphore);
    std::cout << "Thread: " << id << " terminated" << std::endl;

    return 0;
}


//==============================================================================
int main(int argc, char* argv[])
{
    // Create/Open a named mutex
    bool bAlreadyExists = false;
    pSemaphore = new vpl::sys::CNamedSemaphore(1, &SEMAPHORE_NAME, &bAlreadyExists);
    if( bAlreadyExists )
    {
        std::cout << "Semaphore already exists." << std::endl;
    }

    // Threads
    vpl::sys::CThread *ppThreads[THREADS];
    int piThreadsId[THREADS];

    // Creation of all threads
    for( int i = 0; i < THREADS; i++ )
    {
        piThreadsId[i] = i;
        ppThreads[i] = new vpl::sys::CThread(thread, (void *)&piThreadsId[i], true);
    }

    // Sleep
    vpl::sys::sleep(10000);

    // Destroy all ppThreads
    for( int i = 0; i < THREADS; i++ )
    {
        ppThreads[i]->terminate(true, 1000);
        delete ppThreads[i];
    }

    // Delete global variables
    delete pSemaphore;

    return 0;
}

