//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2003/12/04                       
 * 
 * Description:
 * - Testing of the vpl::CVector, CMatrix and CQuaternion templates used to compute geometric transformations.
 */

#include <VPL/Math/Vector.h>
#include <VPL/Math/TransformMatrix.h>
#include <VPL/Math/Quaternion.h>

//==============================================================================
/*!
 * Prints a given vector
 */
void printVector(const vpl::math::CDVector3& v)
{
    std::cout.setf(std::ios_base::fixed);
    std::cout << "  ";
    for( vpl::tSize i = 0; i < v.getSize(); i++ )
    {
        std::cout << v(i) << " ";
    }
    std::cout << std::endl;
}

//==============================================================================
/*!
 * Prints a given quaternion
 */
void printQuaternion(const vpl::math::CDQuat& v)
{
    std::cout.setf(std::ios_base::fixed);
    std::cout << "  ";
    for( vpl::tSize i = 0; i < 4; i++ )
    {
        std::cout << v(i) << " ";
    }
    std::cout << std::endl;
}

//==============================================================================
/*!
 * Prints a given matrix
 */
void printMatrix(const vpl::math::CDTransformMatrix & m)
{
    std::cout.setf(std::ios_base::fixed);
    for( vpl::tSize j = 0; j < 4; ++j )
    {
        std::cout << "  ";
        for( vpl::tSize i = 0; i < 4; ++i )
        {
            std::cout << m(j, i) << " ";
        }
        std::cout << std::endl;
    }
}


//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    vpl::math::CDVector3 v1(1.0, 1.0, 3.0);
    std::cout << "Vector 1" << std::endl;
    printVector(v1);
    std::cout << "Opposite:" << std::endl;
    printVector(-v1);
    keypress();


    std::cout << "Length: " << v1.length() << ", squared length: " << v1.length2() << std::endl;
    v1.normalize();
    std::cout << "Normalized length: " << v1.length() << std::endl;
    std::cout << "Normalized vector: " << std::endl;
    printVector(v1);
    keypress();

    std::cout << "Piecewise operations:" << std::endl;
    std::cout << "Multiply 6:" << std::endl;
    v1 = v1 * 2.0;
    v1 = vpl::CScalard(3.0) * v1;

    printVector(v1);
    keypress();

    v1.set(1.0, 0.0, 0.0);
    std::cout << "Vector 1" << std::endl;
    printVector(v1);
    vpl::math::CDVector3 v2(0.0, 1.0, 0.0);
    std::cout << "Vector 2" << std::endl;
    printVector(v2);
    keypress();

    std::cout << "Dot product with vector 1: " << v1*v2 << std::endl;
    vpl::math::CDVector3 v3(v1 ^ v2);
    std::cout << "Cross product v1 x v2: ";
    printVector(v3);
    keypress();

    std::cout << "Quaternion" << std::endl;
    vpl::math::CDQuat q1(vpl::math::HALF_PI, v3);
    printQuaternion(q1);

    std::cout << "Rotate vector 1: " << std::endl;
    printVector(v1);
    v3 = q1 * v1;
    printVector(v3);
    keypress();    

    std::cout << "Identity matrix: " << std::endl;
    vpl::math::CDTransformMatrix tm;
    printMatrix(tm);

    std::cout << "Inverted matrix: " << std::endl;
    printMatrix(vpl::math::CDTransformMatrix::inverse(tm));

    std::cout << "Vector: " << std::endl;
    v1.set(1.0, 0.0, 0.0);
    printVector(v1);

    std::cout << "Transformed: " << std:: endl;
    printVector(tm * v1);
    keypress();

    std::cout << "Translation matrix: " << std::endl;
    tm.setTrans(vpl::math::CDVector3(2.0, 1.0, 0.0));
    printMatrix(tm);
    std::cout << "Transformed: " << std:: endl;
    printVector(tm * v1);
    keypress();

    std::cout << "Rotation matrix: " << std::endl;
    tm.makeRotate(q1);
    printMatrix(tm);
    std::cout << "Transformed: " << std:: endl;
    printVector(tm * v1);
    keypress();

    std::cout << "Scale matrix: " << std::endl;
    tm.makeScale(3.0, 2.0, 0.0);
    printMatrix(tm);
    std::cout << "Transformed: " << std:: endl;
    printVector(tm * v1);
    keypress();

    std::cout << "Rotation and then translation matrix: " << std::endl;
    tm = vpl::math::CDTransformMatrix(q1);
    tm = vpl::math::CDTransformMatrix::translate(vpl::math::CDVector3(2.0, 1.0, 0.0)) * tm;
    printMatrix(tm);
    std::cout << "Transformed: " << std:: endl;
    printVector(tm * v1);
    keypress();

    std::cout << "Pre mult tests. Input vector: " << std::endl;
    printVector(v1);
    tm.unit();
    tm.preMultTranslate(vpl::math::CDVector4(2.0, 1.0, 3.0));
    std::cout << "Translate by (2.0, 1.0, 3.0): " << std::endl;
    printVector(tm * v1);
    tm.preMultRotate(q1);
    std::cout << "Rotate from x to y axis: " << std::endl;
    printVector(tm * v1);
   tm.preMultScale(vpl::math::CDVector4(2.0, -1.0, 3.0));
    std::cout << "Scale by (2.0, -1.0, 3.0): " << std::endl;
    printVector(tm * v1);
    keypress();

    std::cout << "Post mult tests. Input vector: " << std::endl;
    printVector(v1);
    tm.unit();
    tm.postMultTranslate(vpl::math::CDVector4(2.0, 1.0, 3.0));
    std::cout << "Translate by (2.0, 1.0, 3.0): " << std::endl;
    printVector(tm * v1);
    tm.postMultRotate(q1);
    std::cout << "Rotate from x to y axis: " << std::endl;
    printVector(tm * v1);
    tm.postMultScale(vpl::math::CDVector4(2.0, -1.0, 3.0));
    std::cout << "Scale by (2.0, -1.0, 3.0): " << std::endl;
    printVector(tm * v1);
    keypress();

    std::cout << "Decomposition test" << std::endl;
    tm.unit();
    tm.postMultTranslate(vpl::math::CDVector4(2.0, 1.0, 3.0));
    std::cout << "Translate by (2.0, 1.0, 3.0): " << std::endl;
    tm.postMultRotate(q1);
    std::cout << "Rotate from x to y axis: " << std::endl;
    tm.postMultScale(vpl::math::CDVector4(1.0, 2.0, 3.0));
    std::cout << "Scale by (1.0, 2.0, 3.0): " << std::endl;

    vpl::math::CDTransformMatrix trans, rot, scale;
    tm.decompose(trans, rot, scale);
    std::cout << "Translation matrix: " << std::endl;
    printMatrix(trans);
    std::cout << "Rotation matrix: " << std::endl;
    printMatrix(rot);
    std::cout << "Scale matrix: " << std::endl;
    printMatrix(scale);

    vpl::math::CDVector3 tv, sv;
    vpl::math::CDQuat rq;
    tm.decompose(tv, rq, sv);
    std::cout << "Translation vector: " << std::endl;
    printVector(tv);
    std::cout << "Rotation quaternion: " << std::endl;
    printQuaternion(rq);
    std::cout << "Uniform scale vector: " << std::endl;
    printVector(sv);
    keypress();

}