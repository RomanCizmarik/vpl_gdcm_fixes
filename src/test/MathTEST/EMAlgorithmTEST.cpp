//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2006 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2006/09/15                       
 *
 * Description:
 * - Testing of the vpl::math::CMaxLikelihoodEstimationByEM template.
 */

#include <VPL/Math/Random.h>
#include <VPL/Math/Vector.h>
#include <VPL/Math/VectorFunctions.h>

// Enable logging
#define EM_LOGGING_ENABLED

#include <VPL/Math/Algorithm/EM.h>

// STL
#include <iostream>


//==============================================================================
/*
 * Global constants.
 */

//! Number of input samples.
const vpl::tSize NUM_OF_SAMPLES = 1000;

//! Gaussian parameters.
const double M1 = 0.0;
const double S1 = 5.0;
const double M2 = -30.0;
const double S2 = 5.0;
const double M3 = 50.0;
const double S3 = 10.0;


//==============================================================================
/*!
 * Prints a given vector.
 */
void printVector(vpl::math::CDVector& v)
{
    std::cout.setf(std::ios_base::fixed);
    std::cout << "  ";
    for( vpl::tSize i = 0; i < v.getSize(); i++ )
    {
        std::cout << v(i) << " ";
    }
    std::cout << std::endl;
}


//==============================================================================
/*!
 * Waiting for a key.
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * main()
 */
int main(int argc, const char *argv[])
{
    // Init global log
    VPL_LOG_ADD_CERR_APPENDER;
//    VPL_LOG_ADD_FILE_APPENDER("temp.log");

    // Random numbers generator
    vpl::math::CNormalPRNG Random;
    std::cout << "Random number generator parameters:" << std::endl;
    std::cout << "  Gaussian 1: Mean = " << M1 << ", Sigma = " << S1 << std::endl;
    std::cout << "  Gaussian 2: Mean = " << M2 << ", Sigma = " << S2 << std::endl;
    std::cout << "  Gaussian 3: Mean = " << M3 << ", Sigma = " << S3 << std::endl;

    // One third of all samples
    vpl::tSize THIRD = NUM_OF_SAMPLES / 3;

    // Generate random data
    std::cout << "Vector 1" << std::endl;
    vpl::math::CDVector v1(NUM_OF_SAMPLES);
    v1.fill(1);
    for( vpl::tSize i = 0; i < NUM_OF_SAMPLES; ++i )
    {
        if( i < THIRD )
        {
            v1(i) = Random.random(M1, S1);
        }
        else if( i < 2 * THIRD )
        {
            v1(i) = Random.random(M2, S2);
        }
        else
        {
            v1(i) = Random.random(M3, S3);
        }
    }
    printVector(v1);
    keypress();

    vpl::math::CMaxLikelihoodByEM<vpl::math::CDVector,1> Clustering;
//    if( !Clustering.execute(v1, 3) )
    if( !Clustering.execute(v1) )
    {
        std::cout << "Error: EM algorithm failed!" << std::endl;
        return 0;
    }

    vpl::math::CMaxLikelihoodByEM<vpl::math::CDVector,1>::tComponent c;
    std::cout << "Number of components: " << Clustering.getNumOfComponents() << std::endl;
    for( vpl::tSize i = 0; i < Clustering.getNumOfComponents(); ++i )
    {
        c = Clustering.getComponent(i);
        std::cout << "  Component mean " << i << ": " << c.getMean() << std::endl;
        std::cout << "            cov  " << i << ": " << c.getCov().at(0) << std::endl;
    }

    vpl::math::CMaxLikelihoodByEM<vpl::math::CDVector,1>::tVector v;
    std::cout << "Membership function:"<< std::endl;
    Clustering.getMembership(150, v);
    std::cout << "  Sample 150 (value = " << v1(150) << "): " << v << std::endl;
    Clustering.getMembership(450, v);
    std::cout << "  Sample 450 (value = " << v1(450) << "): " << v << std::endl;
    Clustering.getMembership(750, v);
    std::cout << "  Sample 750 (value = " << v1(750) << "): " << v << std::endl;

    return 0;
}

