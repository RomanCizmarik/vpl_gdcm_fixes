//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2004/11/22                       
 *
 * Description:
 * - Testing of the vpl::CSingleton template.
 */

#include <VPL/Base/Singleton.h>

// STL
#include <iostream>


//==============================================================================
/*!
 * Singleton CA.
 * - Short longevity.
 * - Public constructor allows user instantiation!
 */
class CA : public vpl::base::CSingleton<vpl::base::SL_SHORT>
{
public:
    //! Default constructor
    CA() { std::cout << "  CA::CA()" << std::endl; }

    //! Destructor
    ~CA() { std::cout << "  CA::~CA()" << std::endl; }

    //! Print message
    void print() { std::cout << "  CA::print()" << std::endl; }

};


//==============================================================================
/*!
 * Singleton CAA - short longevity.
 * - User cannot make instances :)
 */
class CAA : public vpl::base::CSingleton<vpl::base::SL_SHORT>
{
public:
    //! Destructor
    ~CAA() { std::cout << "  CAA::~CAA()" << std::endl; }

    //! Print a message
    void print() { std::cout << "  CAA::print()" << std::endl; }

private:
    //! Private constructor
    CAA() { std::cout << "  CAA::CAA()" << std::endl; }

    VPL_PRIVATE_SINGLETON(CAA);
};


//==============================================================================
/*!
 * Singleton CB.
 * - Middle longevity.
 * - Public constructor allows user instantiation!
 */
class CB : public vpl::base::CSingleton<vpl::base::SL_MIDDLE>
{
public:
    //! Default constructor.
    CB() { std::cout << "  CB::CB()" << std::endl; }

    //! Destructor.
    ~CB() { std::cout << "  CB::~CB()" << std::endl; }

    //! Print message.
    void print() { std::cout << "  CB::print()" << std::endl; }

};


//==============================================================================
/*!
 * Singleton CC.
 * - Long longevity.
 * - Public constructor allows user instantiation!
 */
class CC : public vpl::base::CSingleton<vpl::base::SL_LONG>
{
public:
    //! Default constructor.
    CC() { std::cout << "  CC::CC()" << std::endl; }

    //! Destructor.
    ~CC() { std::cout << "  CC::~CC()" << std::endl; }

    //! Print message.
    void print() { std::cout << "  CC::print()" << std::endl; }

};


//==============================================================================
/*!
 * Singleton CCC.
 * - Long longevity.
 * - User cannot make instances :)
 */
class CCC : public vpl::base::CSingleton<vpl::base::SL_LONG>
{
public:
    //! Destructor.
    ~CCC() { std::cout << "  CCC::~CCC()" << std::endl; }

    //! Print message.
    void print() { std::cout << "  CCC::print()" << std::endl; }

private:
    //! Private constructor.
    CCC() { std::cout << "  CCC::CCC()" << std::endl; }

    VPL_PRIVATE_SINGLETON(CCC);
};


//==============================================================================
/*!
 * Singleton CD.
 * - Default longevity.
 * - Public constructor allows user instantiation!
 */
class CD : public vpl::base::CDefaultSingleton
{
public:
    //! Public constructor.
    CD() { std::cout << "  CD::CD()" << std::endl; }

    //! Destructor.
    ~CD() { std::cout << "  CD::~CD()" << std::endl; }

    //! Print message.
    void print() { std::cout << "  CD::print()" << std::endl; }

};


//==============================================================================
/*!
 * Singleton CS.
 * - Library longevity.
 * - Public constructor allows user instantiation!
 */
class CS : public vpl::base::CLibrarySingleton
{
public:
    //! Public constructor.
    CS() { std::cout << "  CS::CS()" << std::endl; }

    //! Destructor.
    ~CS() { std::cout << "  CS::~CS()" << std::endl; }

    //! Print message.
    void print() { std::cout << "  CS::print()" << std::endl; }

};


//==============================================================================
/*!
 * Waiting for a key.
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * Gets the singleton instances.
 */
void getInstances()
{
    // Get the singleton instances
    CS& S = VPL_SINGLETON(CS);
    CA& A = VPL_SINGLETON(CA);
    CC& C = VPL_SINGLETON(CC);
    CB& B = VPL_SINGLETON(CB);
    CD& D = VPL_SINGLETON(CD);
    CAA& AA = VPL_SINGLETON(CAA);
    CCC& CC = VPL_SINGLETON(CCC);

    // Call the print() method
    S.print();
    A.print();
    C.print();
    B.print();
    D.print();
    AA.print();
    CC.print();
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    std::cout << "Get Singleton Instances 1" << std::endl;
    getInstances(); keypress();

    std::cout << "Get Singleton Instances 2" << std::endl;
    getInstances(); keypress();

    std::cout << "Exit" << std::endl;

    //vpl::base::destroyAllSingletons();

    return 0;
}

