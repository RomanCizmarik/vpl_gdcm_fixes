//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2004/11/22                       
 *
 * Description:
 * - Testing of the vpl::CScopedPtr template.
 */

#include <VPL/Base/ScopedPtr.h>

// STL
#include <iostream>


//==============================================================================
/*!
 * Class CA
 */
class CA
{
public:
    //! Allow class instantiation by smart pointer
    VPL_SCOPEDPTR(CA);

public:
    //! Internal data
    int m_iData;

public:
    //! Constructor
    CA() : m_iData(0)
    {
        std::cout << "  <" << this << "> CA::CA()" << std::endl;
    }

    //! Constructor
    CA(int iData) : m_iData(iData)
    {
        std::cout  << "  <" << this << "> CA::CA(" << iData << ")" << std::endl;
    }

    //! Constructor
    CA(int *piData) : m_iData(*piData)
    {
        std::cout  << "  <" << this << "> CA::CA(" << *piData << ")" << std::endl;
    }

    //! Copy constructor
    CA(const CA& a) : m_iData(a.m_iData)
    {
        std::cout  << "  <" << this << "> CA::CA(" << &a << ")" << std::endl;
    }

    //! Destructor
    ~CA()
    {
        std::cout << "  <" << this << "> CA::~CA()" << std::endl;
    }

    //! Print message
    void print()
    {
        std::cout << "  <" << this << "> CA::m_iData = " << m_iData << std::endl;
    }
};

//! Base pointer to the class CA
typedef vpl::base::CScopedPtr<CA> CAPtr;


//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    std::cout << "Create CScopedPtr<CA> p1 using CA() default constructor" << std::endl;
    CAPtr p1(new CA());
    p1->print();
    keypress();

    std::cout << "Create p2 using CA(const int) constructor (value = 10)" << std::endl;
    {
        CAPtr p2(new CA(10));
        p2->print();
        keypress();
    }

    std::cout << "Print p1" << std::endl;
    p1->print();
    keypress();

    std::cout << "Create p2 using CA(const int *) constructor (value = 20)" << std::endl;
    {
        int data = 20;
        CAPtr p2(new CA(&data));
        p2->print();
        keypress();
    }

    std::cout << "Create p2 using CScopedPtr::CScopedPtr(const CScopedPtr&) from p1" << std::endl;
    {
        CAPtr p2(p1);
        p2->print();
        keypress();

        std::cout << "Increment data of the p2" << std::endl;
        p2->m_iData += 1;
        p2->print();
        keypress();
    }

    std::cout << "Recreate p1 using the assignment operator (value = 30)" << std::endl;
    p1 = new CA(30);
    p1->print();
    keypress();

    std::cout << "Print p1" << std::endl;
    p1->print();
    keypress();

    std::cout << "Create CScopedPtr p2 using copy constructor CA::CA(const CA&)" << std::endl;
    {
        CAPtr p2(new CA(*p1));
        p2->print();
        keypress();

        std::cout << "Increment data of the p2" << std::endl;
        p2->m_iData += 1;
        p2->print();
        keypress();
    }

    std::cout << "Print p1" << std::endl;
    p1->print();
    keypress();

    std::cout << "Exit" << std::endl;

    return 0;
}

